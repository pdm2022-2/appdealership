package com.example.appdealership.enum

enum class ETipoVeiculo (val tipo: String){
    HATCH("Hatch"),
    CAMINHAO("Caminhao"),
    MOTO ("Motocicleta"),
    SEDAN ("Sedan"),
    CAMINHONETE ("Caminhonete"),
    VAN ("Van"),
    SUV ("SUV")
}