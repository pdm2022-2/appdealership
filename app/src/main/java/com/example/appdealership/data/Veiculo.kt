package com.example.appdealership.data

import com.example.appdealership.enum.ETipoVeiculo

data class Veiculo (
    var modelo: String,
    var preco: Float,
    var tipoVeiculo: ETipoVeiculo,
    var vendido: Boolean = false
        )