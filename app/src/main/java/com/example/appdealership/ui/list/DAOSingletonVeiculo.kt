package com.example.appdealership.ui.list

import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.snapshots.SnapshotStateList
import com.example.appdealership.data.Veiculo

object DAOSingletonVeiculo {
    private var listaVeiculos: SnapshotStateList<Veiculo> = mutableStateListOf()

    fun getListaVeiculos(): MutableList<Veiculo>{
        return this.listaVeiculos
    }

    fun adicionaVeiculo(veiculo: Veiculo){
        this.listaVeiculos.add(veiculo)
    }

}