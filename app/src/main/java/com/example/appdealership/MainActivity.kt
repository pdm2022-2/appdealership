package com.example.appdealership

import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.ArrowDropDown
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.modifier.modifierLocalConsumer
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.appdealership.data.Veiculo
import com.example.appdealership.enum.ETipoVeiculo
import com.example.appdealership.ui.list.DAOSingletonVeiculo
import com.example.appdealership.ui.theme.AppDealerShipTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            BuildLayout()
        }
    }
}

@Composable
private fun BuildLayout() {

    AppDealerShipTheme {
        // A surface container using the 'background' color from the theme
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colors.background
        ) {
            Column(modifier = Modifier.padding(8.dp)) {
                val listaVeiculos = DAOSingletonVeiculo.getListaVeiculos()
                CardAdicionaVeiculos(listaVeiculos)
                ListaVeiculos(listaVeiculos)
            }

        }
    }
}

@Composable
fun CardAdicionaVeiculos(veiculos: MutableList<Veiculo>) {
    var modelo by remember { mutableStateOf("") }
    var tipo by remember { mutableStateOf("") }
    var preco by remember { mutableStateOf("") }
    var tipoVeiculo: ETipoVeiculo = ETipoVeiculo.HATCH

    val listaTipos = listOf(
        ETipoVeiculo.HATCH,
        ETipoVeiculo.CAMINHAO,
        ETipoVeiculo.MOTO,
        ETipoVeiculo.SEDAN,
        ETipoVeiculo.CAMINHONETE,
        ETipoVeiculo.VAN,
        ETipoVeiculo.SUV
    )

    Column() {
        Card() {
            Column() {
                OutlinedTextField(
                    value = modelo,
                    onValueChange = { modelo = it },
                    label = { Text("Modelo") },
                    placeholder = { Text(text = "Modelo...")},
                    modifier = Modifier
                        .fillMaxWidth()
                        .align(Alignment.CenterHorizontally)
                        .padding(horizontal = 8.dp)
                )

                TipoVeiculoSpinner(
                    listaTipos,
                    valorSetado = ETipoVeiculo.HATCH,
                    onSelectionChanged = { selecionado ->
                        tipoVeiculo = selecionado
                    }
                )

                OutlinedTextField(
                    value = preco,
                    onValueChange = { preco = it },
                    label = { Text("Preço") },
                    placeholder = { Text(text = "Preço...")},
                    modifier = Modifier
                        .fillMaxWidth()
                        .align(Alignment.CenterHorizontally)
                        .padding(horizontal = 8.dp),
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                )
                Button(
                    modifier = Modifier.align(Alignment.CenterHorizontally),
                    onClick = {
                        if(modelo.trim() != "" && preco.trim() != ""){
                            var veiculo = Veiculo(modelo,preco.toFloat(), tipoVeiculo)
                            modelo = ""
                            tipo = ""
                            preco = ""
                            veiculos.add(veiculo)
                        }


                    }
                ) {
                    Text(text = "Enviar")
                }
            }
        }
    }
}

@Composable
fun TipoVeiculoSpinner(
    list: List<ETipoVeiculo>,
    valorSetado: ETipoVeiculo,
    onSelectionChanged: (selecao: ETipoVeiculo) -> Unit
) {

    var selecionado by remember { mutableStateOf(valorSetado) }
    var expandido by remember { mutableStateOf(false) } // initial value

    Box (modifier = Modifier.padding(horizontal = 8.dp)){
        Column {
            OutlinedTextField(
                value = (selecionado.tipo),
                onValueChange = { },
                label = { Text(text = "Tipo de veiculo") },
                modifier = Modifier.fillMaxWidth(),
                trailingIcon = { Icon(Icons.Outlined.ArrowDropDown, null) },
                readOnly = true
            )
            DropdownMenu(
                modifier = Modifier.fillMaxWidth(),
                expanded = expandido,
                onDismissRequest = { expandido = false },
            ) {
                list.forEach { entry ->

                    DropdownMenuItem(
                        modifier = Modifier.fillMaxWidth(),
                        onClick = {
                            selecionado = entry
                            expandido = false
                            onSelectionChanged(selecionado)
                        }
                    ){
                            Text(
                                text = (entry.tipo),
                                modifier = Modifier
                                    .wrapContentWidth()
                                    .align(Alignment.CenterVertically)
                            )

                    }
                }
            }
        }

        Spacer(
            modifier = Modifier
                .matchParentSize()
                .background(Color.Transparent)
                .padding(10.dp)
                .clickable(
                    onClick = { expandido = !expandido }
                )
        )
    }
}

@Composable
fun ListaVeiculos(veiculos: MutableList<Veiculo>){
LazyColumn{
        items(veiculos){ veiculo ->
            ItemViewVeiculo(veiculo = veiculo)
        }
    }
}

@Composable
fun ItemViewVeiculo(veiculo: Veiculo) {
    var expande by remember {
        mutableStateOf(false)
    }
    var vendido by remember {
        mutableStateOf(false)
    }
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .pointerInput(Unit) {
                detectTapGestures(
                    onTap = { expande = !expande },
                    onLongPress = { vendido = !vendido
                                    veiculo.vendido = vendido
                                  },
                )
            }
            .padding(2.dp)
        ) {

        Row() {
            if (!vendido){
                Text(text = "  ", modifier = Modifier.background(Color(0xFF3EA80F)))
                Column() {
                    Text(text = veiculo.modelo, modifier = Modifier.padding(horizontal = 4.dp))
                    AnimatedVisibility(visible = expande) {
                        Column() {
                            Text(text = "Tipo: " + veiculo.tipoVeiculo.toString(), modifier = Modifier.padding(horizontal = 4.dp))

                            Text(text = "Preço: " + veiculo.preco.toString(), modifier = Modifier.padding(horizontal = 4.dp))

                            Text(text = "Status: Este veiculo está disponivel")

                        }

                    }
                }
            }else{
                Text(text = "  ", modifier = Modifier.background(Color(0xFFA80F0F)))
                Column() {
                    Text(text = veiculo.modelo, modifier = Modifier.padding(horizontal = 4.dp))
                    AnimatedVisibility(visible = expande) {
                        Column() {
                            Text(text = "Tipo: " + veiculo.tipoVeiculo.toString(), modifier = Modifier.padding(horizontal = 4.dp))

                            Text(text = "Preço: " + veiculo.preco.toString(), modifier = Modifier.padding(horizontal = 4.dp))

                            Text(text =  "Status: Este veiculo nao está disponível" )

                        }

                    }
                }
            }


        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    BuildLayout()
}